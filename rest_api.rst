.. _rest-api:

REST API
********

The REST API can be used directly or through the `Python wrapper <python>`__. It consists of different endpoints listed below.

.. _rest-api-search:

``POST /search``
================

Queries the DataMart system for datasets.

The ``Content-Type`` should be set to `multipart/form-data <https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Disposition>`__ to allow sending both the query description and the data file.

The following keys are accepted in the request body (you need to specify at least one key):

* ``data``: path to a D3M dataset **OR** path to a CSV file **OR** CSV file contents
* ``query``: JSON object representing the query, according to :ref:`the Query API specification <query-input>`

This endpoint returns the following JSON object:

.. code:: javascript

   {
     "results": [ {result object}, {result object}, ... ]
   }

where each `result` object has the following keys:

* `id`: dataset id
* `score`: the score given to this dataset
* `metadata`: varying metadata from this dataset
* `join_columns`: pairs of columns that can be joined (if augmentation is a join)
* `union_columns`: pairs of columns that can be unioned (if augmentation is a union)

.. note:: TODO: Standardize `metadata` format

.. note:: TODO: Standardize join/union information, provide joinability score

Either `join_columns` or `union_columns` will be present, which indicates the augmentation task. For each pair of columns, the first column corresponds to the input data, and the second column corresponds to the query result.

.. _rest-api-download:

``GET /download/id``
====================

Downloads a dataset from DataMart, where `id` is the dataset identifier from the `result` JSON object.

It accepts one query parameter, `format`, which indicates the format of the returned file. The options are:

* `csv` (``/download/mydatasetid?format=csv``): returns the dataset as a CSV file (``application/octet-stream``). This is the default.
* `d3m` (``/download/mydatasetid?format=d3m``): returns a zip file (``application/zip``) containing the dataset as a CSV file and its corresponding ``datasetDoc.json`` file. The structure follows the D3M format:

  .. code::

     dataset.zip
     +-- datasetDoc.json
     +-- tables
         +-- learningData.csv

.. note:: TODO: Standardize ``?format=`` parameter

.. note:: TODO: Get rid of `id`, use arbitrary JSON objects to represent materialization information (ISI). This means it should probably be a ``POST /download`` instead.

.. _rest-api-augment:

``POST /augment``
=================

.. note:: TODO: Is this still required?
