.. _query_result_sample:

Query Result JSON Sample
************************

.. literalinclude:: query_result_sample.json
   :language: json
   :linenos:
