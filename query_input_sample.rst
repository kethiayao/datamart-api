.. _query_input_sample:

Query Input JSON Samples
************************

.. literalinclude:: query_input_sample.json
   :language: json
   :linenos:

.. literalinclude:: query_input_sample_taxi.json
   :language: json
   :linenos:

.. literalinclude:: query_input_sample_fifa.json
   :language: json
   :linenos:

.. literalinclude:: query_input_sample_hof.json
   :language: json
   :linenos:
