import abc
import datetime
import importlib
import typing

import d3m
import d3m.container
import d3m.metadata.base as metadata_base
from d3m import utils as utils

"""
Classes to define D3M Datamart.
"""

__all__ = ('DatamartQueryCursor', 'D3MDatamart', 'DatasetColumn', 'DatamartSearchResult', 'D3MAugmentSpec',
           'D3MJoinSpec', 'D3MUnionSpec', 'TemporalGranularity', 'GeospatialGranularity', 'ColumnRelationship', 'DatamartQuery',
           'VariableConstraint', 'NamedEntityVariable', 'TemporalVariable', 'GeospatialVariable', 'DataframeVariable')

D = typing.TypeVar('D', bound='Datamart')
Q = typing.TypeVar('Q', bound='DatamartQuery')
R = typing.TypeVar('R', bound='DatamartSearchResult')
C = typing.TypeVar('C', bound='VariableConstraint')
S = typing.TypeVar('S', bound='D3MAugmentSpec')


class DatamartQueryCursor(abc.ABC, typing.Iterator[R]):
    """
    Cursor to iterate through datamart search results.
    """

    @abc.abstractmethod
    def get_all(self) -> typing.List[R]:
        """
        Return a list of all DatamartSearchResults.
        """
        pass


class D3MDatamart(abc.ABC):
    """
    All datamarts must implement this abstract class.
    """

    @abc.abstractmethod
    def __init__(self, connection_url: str):
        """
        Initializes a Datamart instance.

        Parameters
        ----------
        connection_url: str
            A connection string used to identify a specific datamart implementation.
        Raises
        ------
        ValueError
            Datamart implementation does not recognize the connection url

        """
        pass

    @classmethod
    @abc.abstractmethod
    def get_connection_url(cls) -> str:
        """
        Returns
        -------
        str
            The connection string for datarmart implementation
        """
        pass

    @classmethod
    def get_instances(cls) -> typing.List[D]:
        return [subclass(subclass.get_connection_url()) for subclass in D3MDatamart.__subclasses__()]

    @abc.abstractmethod
    def search(self, query: Q, *, timeout=None, limit: int = 20) -> DatamartQueryCursor:
        """This entry point supports search using a query specification.

        The query specification supports quering datasets by keywords, named entities, temporal ranges, and geospatial ranges.

        Parameters
        ----------
        query: DatamartQuery
            Query specification.
        timeout: int
            Maximum number of seconds before returning results.
        limit: int
            Maximum number of search results to return.

        returns
        -------
        DatamartQueryCursor
            A cursor pointing to search results.

        """
        pass

    @abc.abstractmethod
    def search_with_data(self, query: Q, supplied_data: d3m.container.Dataset, *,
                         timeout=None, limit: int = 20) -> DatamartQueryCursor:
        """
        This entry point supports search based on a supplied datasets.
        The query spec is rich enough to enable query by example. The caller can select
        values from a column in a dataset and use the query spec to find datasets that can join with the values
        provided. Use of the query spec enables callers to compose their own "smart search" implementations.

        Types of queries supported include:

        *) Smart search: the caller provides supplied_data (a D3M dataset), and a query containing
        keywords from the D3M problem specification. The search will analyze the supplied data and look for datasets
        that can augment the supplied data in some way. The keywords are used for further filtering and ranking.
        For example, a datamart may try to identify named entities in the supplied data and search for companion
        datasets that contain these entities.

        *) Columns search: this search is similar to smart search in that it uses both query spec and the supplied_data.
        The difference is that query additionally specifies the supplied_data columns using DataframeVariable constraints
        to identify companion datasets.

        Parameters
        ------_---
        query: DatamartQuery
            Query specification
        supplied_data: d3m.container.Dataset
            The data you are trying to augment.
        timeout: int
            Maximum number of seconds before returning results.
        limit: int
            Maximum number of search results to return.


        Returns
        -------
        DatamartQueryCursor
            A cursor pointing to search results containing possible companion datasets for the supplied data.
        """

    @abc.abstractmethod
    def augment(self, query, supplied_data: d3m.container.Dataset, *, timeout: int = None) -> d3m.container.Dataset:
        """
        In this entry point, the caller supplies a query and a dataset, and datamart returns an augmented dataset.
        Datamart automatically determines useful data for augmentation and automatically joins the new data to produce
        an augmented Dataset that may contain new columns and rows, and possibly new dataframes.

        Parameters
        ------_---
        query: DatamartQuery
            Query specification
        supplied_data: d3m.container.Dataset
            The data you are trying to augment.
        timeout: int
            Maximum number of seconds before returning results.

        Returns
        -------
        d3m.container.Dataset
            The augmented Dataset
        """


class DatasetColumn:
    """
    Specify a column of a Dataframe in a D3MDataset
    """

    def __init__(self, resource_id: str, column_index: int):
        self.resource_id = resource_id
        self.column_index = column_index


class DatamartSearchResult(abc.ABC):
    """
    This class represents the search results of a datamart search.
    Different datamarts will provide different implementations of this class.

    Attributes
    ----------
    join_hints: typing.List[D3MAugmentSpec]
        Hints for joining supplied data with datamart data
    """

    @abc.abstractmethod
    def score(self) -> float:
        """
        Returns a non-negative score of the search result.
        Larger scores indicate better matches. Scores across datamart implementions are not comparable.
        """
        pass

    @abc.abstractmethod
    def download(self, supplied_data: d3m.container.Dataset) -> d3m.container.Dataset:
        """
        Produces a D3M dataset (data plus metadata) corresponding to the search result.
        Every time the download method is called on a search result, it will produce the exact same columns
        (as specified in the metadata -- get_metadata), but the set of rows may depend on the supplied_data.
        Datamart is encouraged to return a dataset that joins well with the supplied data, e.g., has rows that match
        the entities in the supplied data. Datamarts may ignore the supplied_data and return the same data regardless.

        If the supplied_data is None, datamarts may return None or a default dataset, based on the search query.

        Paramters
        ---------
        supplied_data: d3m.container.Dataset
            A D3M dataset containing the dataset that is the target for augmentation. Datamart will try to download data
            that augments the supplied data well.
        """
        pass

    @abc.abstractmethod
    def augment(self, supplied_data, augment_spec: S = None, augment_columns: typing.List[DatasetColumn] = None) -> d3m.container.Dataset:
        """
        Produces a D3M dataset that augments the supplied data with data that can be retrieved from this search result.
        The augment methods is a baseline implementation of download plus join. Datamart will use the join_spec if it is
        provided, otherwise will use the join hints specified in the search result and select a join algorithm
        accordingly.

        Callers who want to control over the join process should use the download method and use their own
        join algorithm.

        Paramters
        ---------
        supplied_data: d3m.container.Dataset
            A D3M dataset containing the dataset that is the target for augmentation.
        augment_spec: D3MAugmentSpec
            A specification of columns to use for augmenting the dataset, either adding columns or rows.
        augment_columns: typing.List[DatasetColumn]
            If provided, only the specified columns from the datamart dataset that will be added to the supplied dataset.
        """
        pass

    @abc.abstractmethod
    def get_metadata(self) -> metadata_base.DataMetadata:
        """
        Access the metadata of a search result. The standard JSON specification of Datamart metadata is at query_result_schema.json.

        Returns
        -------
        DataMetadata
            The Datamart metadata of a search result.
        """
        pass

    @abc.abstractmethod
    def get_augment_hints(self, supplied_data: d3m.container.Dataset = None) -> typing.List[S]:
        """
        Returns hints for augmenting supplied data with the data that can be downloaded using this search result.
        In the typical scenario, the hints are based on supplied data that was provided when search was called.

        The optional supplied_data argument enables the caller to request recomputation of hints for specific data.

        Returns
        -------
        typing.List[D3MJoinSpec]
            A list of join hints. Note that datamart is encouraged to return join hints but not required to do so.
        """
        pass

    @abc.abstractmethod
    def serialize(self) -> str:
        """
        Serializes a search result as a string so that it can be put in a pipeline and reconstructed later.

        Returns
        -------
        str
            A string serialization of a search result.
        """
        pass

    @classmethod
    def construct(cls, serialization: str) -> R:
        """
        Reconstructs a DatamartSearchResult from its string serialization.
        This is an advanced method to be used by implementers of primitives that use datamart.
        """
        python_path = serialization.partition('=')[0]
        module_name, _, class_name = python_path.rparition('.')
        module = importlib.import_module(module_name)
        class_ = getattr(module, class_name)
        return class_.construct(serialization)


class D3MAugmentSpec:
    """
    Abstract class for D3M augmentation specifications
    """


class D3MJoinSpec(D3MAugmentSpec):
    """
    A join spec specifies a possible way to join a left dataset with a right dataset. The spec assumes that it may
    be necessary to use several columns in each datasets to produce a key or fingerprint that is useful for joining
    datasets. The spec consists of two lists of column identifiers or names (left_columns, left_column_names and
    right_columns, right_column_names).

    In the simplest case, both left and right are singleton lists, and the expectation is that an appropriate
    matching function exists to adequately join the datasets. In some cases equality may be an appropriate matching
    function, and in some cases fuzz matching is required. The spec join spec does not specify the matching function.

    In more complex cases, one or both left and right lists contain several elements. For example, the left list
    may contain columns for "city", "state" and "country" and the right dataset contains an "address" column. The join
    spec pairs up ["city", "state", "country"] with ["address"], but does not specify how the matching should be done
    e.g., combine the city/state/country columns into a single column, or split the address into several columns.
    """
    def __init__(self, left_resource_id: str, right_resource_id: str, left_columns: typing.List[typing.List[int]],
                 right_columns: typing.List[typing.List[int]]):
        self.left_resource_id = left_resource_id
        self.right_resource_id = right_resource_id
        self.left_columns = left_columns
        self.right_columns = right_columns
        # we can have list of the joining column pairs
        # each list inside left_columns/right_columns is a candidate joining column for that dataFrame
        # each candidate joining column can also have multiple columns


class D3MUnionSpec(D3MAugmentSpec):
    """
    A union spec specifies how to combine rows of a dataframe in the left dataset with a dataframe in the right datset.
    The dataframe after union should have the same columns as the left dataframe.

    Implemntation: TBD
    """
    pass


class TemporalGranularity(utils.Enum):
    YEAR = 1
    MONTH = 2
    DAY = 3
    HOUR = 4
    SECOND = 5


class GeospatialGranularity(utils.Enum):
    COUNTRY = 1
    STATE = 2
    COUNTY = 3
    CITY = 4
    POSTAL_CODE = 5


class ColumnRelationship(utils.Enum):
    CONTAINS = 1
    SIMILAR = 2
    CORRELATED = 3
    ANTI_CORRELATED = 4
    MUTUALLY_INFORMATIVE = 5
    MUTUALLY_UNINFORMATIVE = 6


class DatamartQuery:
    """
    A Datamart query consists of two parts:

    * A list of keywords.

    * A list of required variables. A required variable specifies that a matching dataset must contain a variable
      satisfying the constraints provided in the query. When multiple required variables are given, the matching
      dataset should contain variables that match each of the variable constraints.

    The matching is fuzzy. For example, when a user specifies a required variable spec using named entities, the
    expectation is that a matching dataset contains information about the given named entities. However, due to name,
    spelling, and other differences it is possible that the matching dataset does not contain information about all
    the specified entities.

    In general, Datamart will do a best effort to satisfy the constraints, but may return datasets that only partially
    satisfy the constraints.
    """
    def __init__(self, keywords: typing.List[str], required_variables: typing.List[C]):
        self.keywords = keywords
        self.required_variables = required_variables


class VariableConstraint(abc.ABC):
    """
    Abstract class for all variable constraints.
    """
    pass


class NamedEntityVariable(VariableConstraint):
    """
    Specifies that a matching dataset must contain a variable including the specified set of named entities.
    For example, if the entities are city names, the expectation is that a matching dataset must contain a variable
    (column) with the given city names. Due to spelling differences and incompleteness of datasets, the returned
    results may not contain all the specified entities.

    Parameters
    ----------
    entities: List[str]
        List of strings that should be contained in the matched dataset column.
    """
    def __init__(self, entities: typing.List[str]):
        self.entities = entities


class TemporalVariable(VariableConstraint):
    """
    Specifies that a matching dataset should contain a variable with temporal information (e.g., dates) satifying
    the given constraint. The goal is to return a dataset that covers the requested temporal interval and includes
    data at a requested level of granularity.

    Datamart will return best effort results, including datasets that may not fully cover the specified temporal
    interval or whose granularity is finer or coarser than the requested granularity.

    Parameters
    ----------
    start: datetime
        A matching dataset should contain a variable with temporal information that starts earlier than the given start.
    end: datetime
        A matching dataset should contain a variable with temporal information that ends after the given end.
    granularity: TemporalGranularity
        A matching dataset should provide temporal information at the requested level of granularity.
    """
    def __init__(self, start: datetime.datetime, end: datetime.datetime, granularity: TemporalGranularity = None):
        self.start = start
        self.end = end
        self.granularity = granularity


class GeospatialVariable(VariableConstraint):
    """
    Specifies that a matching dataset should contain a variable with geospatial information that covers the given
    bounding box.

    A matching dataset may contain variables with latitude and longitude information (in one or two columns) that
    cover the given bounding box.

    Alternatively, a matching dataset may contain a variable with named entities of the given granularity that provide
    some coverage of the given bounding box. For example, if the bounding box covers a 100 mile square in Southern
    California, and the granularity is City, the result should contain Los Angeles, and other cities in Southern
    California that intersect with the bounding box (e.g., Hawthorne, Torrance, Oxnard).

    Parameters
    ----------
    lat1: float
        The latitude of the first point
    long1: float
        The longitude of the first point
    lat2: float
        The latitude of the second point
    long2: float
        The longitude of the second point
    granularity: GeospatialGranularity
        Requested geosptial values are well matched with the requested granularity
    """
    def __init__(self, lat1: float, long1: float, lat2: float, long2: float, granularity: GeospatialGranularity = None):
        self.lat1 = lat1
        self.long1 = long1
        self.lat2 = lat2
        self.long2 = long2
        self.granularity = granularity


class DataframeVariable(VariableConstraint):
    """
    Specifies that a matching dataset should contain variables related to given columns in the supplied_dataset.

    The relation ColumnRelationship.CONTAINS specifies that string values in the columns overlap using the string
    equality comparator. If supplied_dataset columns consists of temporal or spatial values, then
    ColumnRelationship.CONTAINS specifies overlap in temporal range or geospatial bounding box, respectively.

    The relation ColumnRelationship.SIMILAR specifies that string values in the columns overlap using fuzzy string matching.

    The relations ColumnRelationship.CORRELATED and ColumnRelationship.ANTI_CORRELATED specify the columns are
    correlated and anti-correlated, respectively.

    The relations ColumnRelationship.MUTUALLY_INFORMATIVE and ColumnRelationship.MUTUALLY_UNINFORMATIVE specify the columns
    are mutually and anti-correlated, respectively.

    Parameters:
    -----------
    columns: typing.List[int]
        Specify columns in the dataframes of the supplied_dataset
    relationship: ColumnRelationship
        Specifies how the the columns in the supplied_dataset are related to the variables in the matching dataset.
    """
    def __init__(self, columns: typing.List[DatasetColumn], relationship: ColumnRelationship):
        self.columns = columns
        self.relationship = relationship
