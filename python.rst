.. _python:

Python Client
*************

The ``datamart`` module is a wrapper for DataMart's REST API. It can be installed using ``pip install datamart``.

Dataset Objects
===============

The :py:class:`datamart.Dataset` object represent a result from a search, and can be used to inspect or download the dataset.

.. autoclass:: datamart.Dataset

   .. automethod:: datamart.Dataset.download

   .. deprecated:: 0.6
      the ``proxy`` parameter. Local materialization support has been removed, using ``proxy=False`` now raises a ``ValueError``.

   .. automethod:: datamart.Dataset.get_augmentation_information

   .. note:: TODO: We've got to decide on the augmentation information

Search
======

.. autofunction:: datamart.search

.. note:: TODO: ``send_data`` should probably default to ``True``

.. note:: TODO: ``timeout`` hasn't been standardized yet

Download
========

.. autofunction:: datamart.download

.. deprecated:: 0.6
   the ``proxy`` parameter. Local materialization support has been removed, using ``proxy=False`` now raises a ``ValueError``.
