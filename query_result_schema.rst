.. _query_result_schema:

Query Output JSON Schema
************************

.. literalinclude:: query_result_schema.json
   :language: json
   :linenos:
