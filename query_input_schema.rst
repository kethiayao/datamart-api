.. _query_input_schema:

Query Input JSON Schema
***********************

.. literalinclude:: query_input_schema.json
   :language: json
   :linenos:
